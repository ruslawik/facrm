<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin'], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');
    Route::get("users/login/{email}/{pass}", "UsersApiController@login");

    // Clients
    Route::apiResource('clients', 'ClientApiController');
    Route::get("clients/search/{email}", "ClientApiController@search");
    Route::get("clients/login/{email}/{pass}", "ClientApiController@login");

    Route::get('sessions/close/{user_id}', "SessionApiController@close");

    // Sessions
    Route::apiResource('sessions', 'SessionApiController');
});


/*

URL                          HTTP Method  Operation
/api/task                     GET          Returns an array of tasks
/api/task/:id                 GET          Returns the task with id of :id
/api/task                     POST         Adds a new task and return it.
/api/task/:id                 PUT          Updates the task with id of :id
/api/task/:id                 DELETE       Deletes the task with id of :id



*/

