<?php

return [
    'date_format'         => 'm/d/Y',
    'time_format'         => 'H:i:s',
    'primary_language'    => 'ru',
    'available_languages' => [
        'ru' => 'Russian',
        'en' => 'English',
        'tr' => 'Turkish',
    ],
];
