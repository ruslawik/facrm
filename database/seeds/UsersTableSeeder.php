<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$lvbJHRrXcpg9pK1dPK5kheH1/K196mM5H/N2D/P1EOnwfwHXq5oi2',
                'remember_token' => null,
                'created_at'     => '2019-10-04 04:47:37',
                'updated_at'     => '2019-10-04 04:47:37',
            ],
        ];

        User::insert($users);
    }
}
