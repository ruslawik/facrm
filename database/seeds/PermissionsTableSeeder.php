<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'         => '1',
                'title'      => 'user_management_access',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '2',
                'title'      => 'permission_create',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '3',
                'title'      => 'permission_edit',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '4',
                'title'      => 'permission_show',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '5',
                'title'      => 'permission_delete',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '6',
                'title'      => 'permission_access',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '7',
                'title'      => 'role_create',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '8',
                'title'      => 'role_edit',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '9',
                'title'      => 'role_show',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '10',
                'title'      => 'role_delete',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '11',
                'title'      => 'role_access',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '12',
                'title'      => 'user_create',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '13',
                'title'      => 'user_edit',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '14',
                'title'      => 'user_show',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '15',
                'title'      => 'user_delete',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '16',
                'title'      => 'user_access',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '17',
                'title'      => 'client_create',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '18',
                'title'      => 'client_edit',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '19',
                'title'      => 'client_show',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '20',
                'title'      => 'client_delete',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '21',
                'title'      => 'client_access',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '22',
                'title'      => 'session_create',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '23',
                'title'      => 'session_edit',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '24',
                'title'      => 'session_show',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '25',
                'title'      => 'session_delete',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
            [
                'id'         => '26',
                'title'      => 'session_access',
                'created_at' => '2019-10-04 04:47:37',
                'updated_at' => '2019-10-04 04:47:37',
            ],
        ];

        Permission::insert($permissions);
    }
}
