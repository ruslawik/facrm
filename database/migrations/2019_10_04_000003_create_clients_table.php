<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();

            $table->string('surname')->nullable();

            $table->string('phone')->nullable();

            $table->string('email');

            $table->string('comment')->nullable();

            $table->string('password');

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
