<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');

            $table->datetime('time_in');

            $table->datetime('time_out');

            $table->integer('ulken');

            $table->integer('bala');

            $table->integer('price_ulken');

            $table->integer('price_bala');

            $table->integer('total')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
