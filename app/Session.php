<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Session extends Model
{
    use SoftDeletes;

    public $table = 'sessions';

    protected $dates = [
        'time_in',
        'time_out',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'bala',
        'ulken',
        'total',
        'time_in',
        'time_out',
        'price_bala',
        'created_at',
        'updated_at',
        'deleted_at',
        'price_ulken',
        'id_client_id',
    ];

    public function id_client()
    {
        return $this->belongsTo(Client::class, 'id_client_id');
    }

    public function getTimeInAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setTimeInAttribute($value)
    {
        $this->attributes['time_in'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function getTimeOutAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setTimeOutAttribute($value)
    {
        $this->attributes['time_out'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }
}
