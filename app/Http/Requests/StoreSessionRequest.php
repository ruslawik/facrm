<?php

namespace App\Http\Requests;

use App\Session;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreSessionRequest extends FormRequest
{
    public function authorize()
    {
        //abort_if(Gate::denies('session_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'id_client_id' => [
                'required',
                'integer',
            ],
            'time_in'      => [
                'required',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
            'time_out'     => [
                //'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
            'ulken'        => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'bala'         => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'price_ulken'  => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'price_bala'   => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'total'        => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
