<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSessionRequest;
use App\Http\Requests\UpdateSessionRequest;
use App\Http\Resources\Admin\SessionResource;
use App\Session;
use Gate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SessionApiController extends Controller
{
    public function index()
    {
        //abort_if(Gate::denies('session_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SessionResource(Session::with(['id_client'])->get());
    }

    public function store(StoreSessionRequest $request)
    {
        $session = Session::create($request->all());

        return (new SessionResource($session))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function close($user_id){

        $now = Carbon::now("Asia/Almaty");
        $updated = Session::where("time_out", NULL)->where("id_client_id", $user_id)->update(["time_out" => $now]);
        if($updated){
            return "true";
        }else{
            return "false";
        }
    }

    public function show(Session $session)
    {
        //abort_if(Gate::denies('session_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SessionResource($session->load(['id_client']));
    }

    public function update(UpdateSessionRequest $request, Session $session)
    {
        $session->update($request->all());

        return (new SessionResource($session))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Session $session)
    {
        //abort_if(Gate::denies('session_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $session->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
