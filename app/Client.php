<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    public $table = 'clients';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'phone',
        'email',
        'surname',
        'comment',
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function sessions()
    {
        return $this->hasMany(Session::class, 'id_client_id', 'id');
    }
}
