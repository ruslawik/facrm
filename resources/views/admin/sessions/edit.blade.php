@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.session.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.sessions.update", [$session->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('id_client_id') ? 'has-error' : '' }}">
                <label for="id_client">{{ trans('cruds.session.fields.id_client') }}*</label>
                <select name="id_client_id" id="id_client" class="form-control select2" required>
                    @foreach($id_clients as $id => $id_client)
                        <option value="{{ $id }}" {{ (isset($session) && $session->id_client ? $session->id_client->id : old('id_client_id')) == $id ? 'selected' : '' }}>{{ $id_client }}</option>
                    @endforeach
                </select>
                @if($errors->has('id_client_id'))
                    <p class="help-block">
                        {{ $errors->first('id_client_id') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('time_in') ? 'has-error' : '' }}">
                <label for="time_in">{{ trans('cruds.session.fields.time_in') }}*</label>
                <input type="text" id="time_in" name="time_in" class="form-control datetime" value="{{ old('time_in', isset($session) ? $session->time_in : '') }}" required>
                @if($errors->has('time_in'))
                    <p class="help-block">
                        {{ $errors->first('time_in') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.session.fields.time_in_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('time_out') ? 'has-error' : '' }}">
                <label for="time_out">{{ trans('cruds.session.fields.time_out') }}</label>
                <input type="text" id="time_out" name="time_out" class="form-control datetime" value="{{ old('time_out', isset($session) ? $session->time_out : '') }}">
                @if($errors->has('time_out'))
                    <p class="help-block">
                        {{ $errors->first('time_out') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.session.fields.time_out_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('ulken') ? 'has-error' : '' }}">
                <label for="ulken">{{ trans('cruds.session.fields.ulken') }}*</label>
                <input type="number" id="ulken" name="ulken" class="form-control" value="{{ old('ulken', isset($session) ? $session->ulken : '') }}" step="1" required>
                @if($errors->has('ulken'))
                    <p class="help-block">
                        {{ $errors->first('ulken') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.session.fields.ulken_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('bala') ? 'has-error' : '' }}">
                <label for="bala">{{ trans('cruds.session.fields.bala') }}*</label>
                <input type="number" id="bala" name="bala" class="form-control" value="{{ old('bala', isset($session) ? $session->bala : '') }}" step="1" required>
                @if($errors->has('bala'))
                    <p class="help-block">
                        {{ $errors->first('bala') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.session.fields.bala_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('price_ulken') ? 'has-error' : '' }}">
                <label for="price_ulken">{{ trans('cruds.session.fields.price_ulken') }}*</label>
                <input type="number" id="price_ulken" name="price_ulken" class="form-control" value="{{ old('price_ulken', isset($session) ? $session->price_ulken : '') }}" step="1" required>
                @if($errors->has('price_ulken'))
                    <p class="help-block">
                        {{ $errors->first('price_ulken') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.session.fields.price_ulken_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('price_bala') ? 'has-error' : '' }}">
                <label for="price_bala">{{ trans('cruds.session.fields.price_bala') }}*</label>
                <input type="number" id="price_bala" name="price_bala" class="form-control" value="{{ old('price_bala', isset($session) ? $session->price_bala : '') }}" step="1" required>
                @if($errors->has('price_bala'))
                    <p class="help-block">
                        {{ $errors->first('price_bala') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.session.fields.price_bala_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('total') ? 'has-error' : '' }}">
                <label for="total">{{ trans('cruds.session.fields.total') }}</label>
                <input type="number" id="total" name="total" class="form-control" value="{{ old('total', isset($session) ? $session->total : '') }}" step="1">
                @if($errors->has('total'))
                    <p class="help-block">
                        {{ $errors->first('total') }}
                    </p>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.session.fields.total_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection