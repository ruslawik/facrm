@extends('layouts.admin')
@section('content')
@can('session_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.sessions.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.session.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.session.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Session">
                <thead>
                    <tr>
                        <th>
                            
                        </th>
                        <th >
                            Время / Сумма
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.id_client') }}
                        </th>
                        <th>
                            {{ trans('cruds.client.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.time_in') }}
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.time_out') }}
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.ulken') }}
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.bala') }}
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.price_ulken') }}
                        </th>
                        <th>
                            {{ trans('cruds.session.fields.price_bala') }}
                        </th>
                        <!--
                        <th>
                            {{ trans('cruds.session.fields.total') }}
                        </th>
                        !-->
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <script>
                    function check_time(t){
                        if(t < 10){
                            t = "0"+t;
                        }
                        return t;
                    }
                    function go_time (div_id, h,m,s){
                        s+=1;
                        if(s==60){
                            m+=1;
                            s=0;
                        }
                        if(m==60){
                            h+=1;
                            m=0;
                        }
                        var g = document.getElementById("time"+div_id).innerHTML = check_time(h)+":"+check_time(m)+":"+check_time(s);
                        setTimeout(go_time, 1000, div_id, h, m, s);
                    }
                    
                </script>
                <tbody>
                    <?php $k = 0; ?>
                    @foreach($sessions as $key => $session)
                    <?php $k++; ?>
                        <tr data-entry-id="{{ $session->id }}" 
                            style="background-color:<?php if($session->time_out != NULL){echo "#91B752";}else{ echo "#E6B055";} ?>;">
                            <td>
                            
                            </td>
                            <td>
                                <div id="time{{$k}}" style="color:blue;"></div>
                                <?php
                                if($session->time_out != NULL){
                                    $totalDuration = Carbon\Carbon::parse($session->time_out)->diffInSeconds($session->time_in);
                                    print(gmdate('H:i:s', $totalDuration));
                                    $minutes = floor($totalDuration/60);
                                    $sum = floor($minutes*$session->ulken*($session->price_ulken/60));
                                    $sum += floor($minutes*$session->bala*($session->price_bala/60));
                                    print("<br>".$sum." тенге");
                                }else{
                                    $now = Carbon\Carbon::now("Asia/Almaty");
                                    $totalDuration = Carbon\Carbon::parse($now)->diffInSeconds($session->time_in);
                                    $hours = floor($totalDuration/3600);
                                    $minutes = floor(($totalDuration - $hours*3600)/60);
                                    $seconds = $totalDuration - ($minutes*60 + $hours*3600);
                                    print("
                                        <script>
                                            go_time(".$k.", ".$hours.", ".$minutes.", ".$seconds.");
                                        </script>
                                        ");
                                }
                                ?>
                            </td>
                            <td>
                                {{ $session->id ?? '' }}
                            </td>
                            <td>
                                {{ $session->id_client->email ?? '' }}
                            </td>
                            <td>
                                {{ $session->id_client->name ?? '' }}
                            </td>
                            <td>
                                {{ $session->time_in ?? '' }}
                            </td>
                            <td>
                                {{ $session->time_out ?? '' }}
                            </td>
                            <td>
                                {{ $session->ulken ?? '' }}
                            </td>
                            <td>
                                {{ $session->bala ?? '' }}
                            </td>
                            <td>
                                {{ $session->price_ulken ?? '' }}
                            </td>
                            <td>
                                {{ $session->price_bala ?? '' }}
                            </td>
                            <!--
                            <td>
                                {{ $session->total ?? '' }}
                            </td>
                            !-->
                            <td>
                                @can('session_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.sessions.show', $session->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('session_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.sessions.edit', $session->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('session_delete')
                                    <form action="{{ route('admin.sessions.destroy', $session->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('session_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.sessions.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 2, 'desc' ]],
    pageLength: 50,
  });
  $('.datatable-Session:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection 