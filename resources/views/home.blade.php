@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
    			<div class="card-header">
        			<h5>Добро пожаловать!</h5>
    			</div>

    			<div class="card-body">
    				Вы вошли в систему управления клиентами и учетом времени компании Family Avenue.
    				<hr>
    				В разделе "Клиенты" Вы можете добавлять, редактировать и удалять информацию о Ваших клиентах.
    				<hr>
    				В разделе "Сессии" Вы увидите все текущие сессии клиентов, запущенные из приложения, а также сможете добавить, редактировать или удалить эти записи.
    			</div>
    		</div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection